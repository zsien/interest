#!/usr/bin/env bash

systemctl stop interest.timer
systemctl disable interest.timer

rm -f /etc/systemd/system/interest.timer
rm -f /etc/systemd/system/interest.service

rm -rf /etc/interest
rm -f /usr/bin/interest

rm -rf /var/lib/interest