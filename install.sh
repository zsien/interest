#!/usr/bin/env bash

install main /usr/bin/interest

install -d /etc/interest
install -m 644 interest/config.json /etc/interest
install -m 644 interest/email.html /etc/interest

install -m 644 interest.service /etc/systemd/system/interest.service
install -m 644 interest.timer /etc/systemd/system/interest.timer

systemctl enable interest.timer
