package main

import (
	"os"
	"io/ioutil"
	"encoding/json"
	"strconv"
	"html/template"
	"time"
	"strings"
	"bufio"
	"fmt"
	"os/exec"
	"bytes"
	"mime"
)

var (
	configFile string = "/etc/interest/config.json"

	config = struct {
		AnnualizedPerMilInterestRate uint64    `json:"annualized_per_mil_interest_rate"`
		PrincipalFile                string    `json:"principal_file"`
		TemplateFile                 string    `json:"template_file"`
		From                         string    `json:"from"`
		To                           string    `json:"to"`
		CC                           string    `json:"cc"`
	}{}

	lastPrincipal uint64

	data = struct {
		Type          uint
		Time          string
		LastPrincipal string
		InterestInfo struct {
			InterestDate string
			LastInterest string
		}
		Transaction struct {
			Content string
			Amount  string
		}
		Repayment struct {
			Amount string
			Remark string
		}
		CurPrincipal string
	}{
		Type:          0,
		Time:          "",
		LastPrincipal: "",
		InterestInfo: struct {
			InterestDate string
			LastInterest string
		}{
			InterestDate: "",
			LastInterest: "",
		},
		Transaction: struct {
			Content string
			Amount  string
		}{
			Content: "",
			Amount:  "",
		},
		Repayment: struct {
			Amount string
			Remark string
		}{
			Amount: "",
			Remark: "",
		},
		CurPrincipal: "",
	}
)

func main() {
	if len(os.Args) < 2 {
		panic("參數錯誤")
	}

	content, err := ioutil.ReadFile(configFile)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(content, &config)
	if err != nil {
		panic(err)
	}

	if _, err := os.Stat(config.PrincipalFile); !os.IsNotExist(err) {
		PrincipalFileContentByteArr, err := ioutil.ReadFile(config.PrincipalFile)
		if err != nil {
			panic(err)
		}

		PrincipalFileContent := strings.TrimSpace(string(PrincipalFileContentByteArr))

		lastPrincipal, err = strconv.ParseUint(PrincipalFileContent, 10, 64)
		if err != nil {
			panic(err)
		}
	}

	data.Time = time.Now().Format("2006年01月02日15:04:05")
	data.LastPrincipal = genNumber(lastPrincipal)

	switch os.Args[1] {
	case "interest":
		genInterest()
	case "transaction":
		addTransaction()
	case "repayment":
		addRepayment()
	default:
		panic("參數錯誤")
	}
}

func genInterest() {
	data.Type = 1
	InterestDate := time.Now().AddDate(0, 0, -1).Format("2006年01月02日")
	lastInterest := lastPrincipal * config.AnnualizedPerMilInterestRate / 1000 / 360
	curPrincipal := lastPrincipal + lastInterest

	setCurPrincipal(curPrincipal)

	data.InterestInfo.InterestDate = InterestDate
	data.InterestInfo.LastInterest = genNumber(lastInterest)

	title := "【賬單】" + InterestDate + "利息入賬通知"
	sendEmail(title)
}

func addTransaction() {
	data.Type = 2

	reader := bufio.NewReader(os.Stdin)

	var (
		transactionContent      string
		transactionAmountString string
	)

	for transactionContent == "" {
		fmt.Print("請輸入交易內容：")
		line, _, _ := reader.ReadLine()
		transactionContent = strings.TrimSpace(string(line))
	}

	for transactionAmountString == "" {
		fmt.Print("請輸入交易金額：")
		line, _, _ := reader.ReadLine()
		transactionAmountString = strings.TrimSpace(string(line))
	}

	transactionAmount := moneyToUnit64(transactionAmountString)
	curPrincipal := lastPrincipal + transactionAmount
	setCurPrincipal(curPrincipal)

	data.Transaction.Content = transactionContent
	data.Transaction.Amount = genNumber(transactionAmount)

	title := "【賬單】" + data.Time + "交易入賬通知"
	sendEmail(title)
}

func addRepayment() {
	data.Type = 3

	reader := bufio.NewReader(os.Stdin)

	var repaymentAmountString string
	var remarkContent string

	for repaymentAmountString == "" {
		fmt.Print("請輸入還款金額：")
		line, _, _ := reader.ReadLine()
		repaymentAmountString = strings.TrimSpace(string(line))
	}

	fmt.Print("請輸入備註：")
	line, _, _ := reader.ReadLine()
	remarkContent = strings.TrimSpace(string(line))

	repaymentAmount := moneyToUnit64(repaymentAmountString)
	curPrincipal := lastPrincipal - repaymentAmount
	setCurPrincipal(curPrincipal)

	data.Repayment.Amount = genNumber(repaymentAmount)
	data.Repayment.Remark = remarkContent

	title := "【賬單】" + data.Time + "還款成功通知"
	sendEmail(title)
}

func setCurPrincipal(curPrincipal uint64) {
	curPrincipalString := genNumber(curPrincipal)
	data.CurPrincipal = curPrincipalString

	saveCurPrincipalString := curPrincipalString + "0"
	saveCurPrincipalString = strings.Replace(saveCurPrincipalString, ".", "", -1)
	err := ioutil.WriteFile(config.PrincipalFile, []byte(saveCurPrincipalString), 0644)
	if err != nil {
		panic(nil)
	}
}

func moneyToUnit64(money string) (uint64Money uint64) {
	moneyLen := len(money)
	dotIndex := strings.Index(money, ".")

	decimalDigits := moneyLen - dotIndex - 1
	if dotIndex == -1 {
		decimalDigits = 0
	}

	padCnt := 7 - decimalDigits
	moneyString := strings.Replace(money, ".", "", -1) + strings.Repeat("0", padCnt)

	uint64Money, err := strconv.ParseUint(moneyString, 10, 64)
	if err != nil {
		panic("解析金額失敗")
	}

	return
}

func genNumber(oriNum uint64) (result string) {
	oriNumString := strconv.FormatUint(oriNum, 10)
	oriNumLen := len(oriNumString)
	lastChar := oriNumString[oriNumLen-1:]
	lastNum, err := strconv.ParseUint(lastChar, 10, 64)
	if err != nil {
		panic("解析最後一個字符失敗")
	}

	newNum := oriNum / 10
	if lastNum >= 5 {
		newNum = (oriNum - lastNum + 10) / 10
	}

	newNumString := strconv.FormatUint(newNum, 10)
	newNumStringLen := len(newNumString)

	if newNumStringLen < 7 {
		padCnt := 7 - newNumStringLen
		newNumString = strings.Repeat("0", padCnt) + newNumString

		newNumStringLen = 7
	}

	result = newNumString[:newNumStringLen-6] + "." + newNumString[newNumStringLen-6:]

	return
}

func sendEmail(title string) {
	tmpl, err := template.ParseFiles(config.TemplateFile)
	if err != err {
		panic("解析模板失敗")
	}

	cmd := exec.Command("sendmail", "-f", config.From, "-t")

	headers := "Subject: " + mime.BEncoding.Encode("utf-8", title) + "\n" +
		"From: " + config.From + "\n" +
		"To: " + config.To + "\n" +
		"CC: " + config.CC + "\n" +
		"Content-Type: text/html; charset=\"utf-8\"\n" +
		"\n"

	var content bytes.Buffer
	tmpl.Execute(&content, data)

	sendmailStdin := headers + content.String()

	stdin, err := cmd.StdinPipe()
	if err != nil {
		panic(err)
	}

	err = cmd.Start()
	if err != nil {
		panic(err)
	}

	stdin.Write([]byte(sendmailStdin))
	stdin.Close()

	err = cmd.Wait()
	if err != nil {
		panic(err)
	}
}
